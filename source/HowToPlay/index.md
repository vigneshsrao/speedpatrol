---
title: How To Play
date: 2019-12-26 22:28:54
---


## Type race

Think you are the fastest at typing around here? Come find out for yourself and win yourself some prizes if you prove it!

Your task is to type as much as you can in a minute. We conclude a round when a fixed number of participants complete the challenge and the fastest one in the round wins the prize.

Note that there will be no type race while a pwn or crypto race is in progress.

## Pwn and cryto race

Think you have got what it takes to tackle exploitation(pwn) and cryptography challenges? Come compete against others who think likewise and takeaway exciting prizes!

For the race, your task will be to solve the given challenge and run `/getflag` file on the server with your handle before the other contestants. For example, if your handle is `luk3`, then you have to run `/getflag luk3` on the server. The first one to do so wins.

While you are solving the challenge you are free to show off your state-of-the-art setup and techniques as your process will be streamed live on the booth monitor with running commentary by members of teambi0s.

Please note that there will be only one crypto and one pwn race in a day. So hurry up and register fast!

![Logo](/img/logo_trans.png)

