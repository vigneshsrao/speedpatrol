---
title: Hall Of Fame
date: 2019-12-27 12:40:45
---

# Pwny Race

| Rank | Name   | Id   |
| -----|:------:| ----:|
| 1    | luk3   | 0x13 |
| 2    | leia   | 0x37 |
| 3    | obi-wan| 0x31 |


# Crypto Race

| Rank | Name   | Id   |
| -----|:------:| ----:|
| 1    | luk3   | 0x13 |
| 2    | leia   | 0x37 |
| 3    | obi-wan| 0x31 |


# Type Race

| Rank | Name   | Id   |
| -----|:------:| ----:|
| 1    | luk3   | 0x13 |
| 2    | leia   | 0x37 |
| 3    | obi-wan| 0x31 |
